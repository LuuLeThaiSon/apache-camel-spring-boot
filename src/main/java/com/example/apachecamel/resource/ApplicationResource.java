package com.example.apachecamel.resource;

import com.example.apachecamel.dto.Order;
import com.example.apachecamel.processor.OrderProcessor;
import com.example.apachecamel.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ApplicationResource extends RouteBuilder {

    private final OrderService service;
    private final OrderProcessor processor;

    @Override
    public void configure() throws Exception {
        restConfiguration().bindingMode(RestBindingMode.json);

        rest().get("/hello-world")
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route()
                .setBody(constant("By SONLLT"))
                .endRest();

        rest().get("/getOrders")
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route()
                .setBody(service::getOrders)
                .endRest();

        rest().post("/addOrder")
                .consumes(MediaType.APPLICATION_JSON_VALUE)
                .type(Order.class)
                .outType(Order.class)
                .route()
                .process(processor)
                .endRest();
    }

}
