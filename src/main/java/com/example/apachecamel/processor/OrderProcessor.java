package com.example.apachecamel.processor;

import com.example.apachecamel.dto.Order;
import com.example.apachecamel.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OrderProcessor implements Processor {
    private final OrderService orderService;

    @Override
    public void process(Exchange exchange) throws Exception {
        orderService.addOrder(exchange.getIn().getBody(Order.class));
    }
}
